// Libc requires _init() to perform some initialization before global
// constructors run. I would love if this symbol is defined as weak in
// newlib-nano (libc), but it is not.
__attribute__((weak)) void _init(void) {}
