#ifndef _SHARED_STORAGE_H_
#define _SHARED_STORAGE_H_

#include <new>
#include <stdint.h>
#include <stdlib.h>
#include <type_traits>

/// @brief Base template, not correctly instanceable
template <typename T, typename Enable = void> struct Serialisator {};

/// @brief Template specialization for basic C-compatable structs
/// T mast be copy-constructible
template <typename T>
struct Serialisator<
    T, typename std::enable_if<std::is_copy_constructible<T>::value>::type> {

  static constexpr size_t getSize() { return sizeof(T); }

  static void serialise(void *dest, const T &src) {
    /* https://stackoverflow.com/a/1554808/8065921
     * Copy-construct new T in dest */
    new (dest) T(src);
  }

  static T deserialise(void *data) { return T(*static_cast<T *>(data)); }
};

/// @brief Storage manager
template <typename T, typename Tpolicy> struct SharedStorageManager {
  using serialisator_t = Serialisator<T>;

  /// @brief store data by storage polcy
  /// @param data - data to store
  bool store(const T &obj) const {
    auto size = serialisator_t::getSize();
    uint8_t buf[size];

    serialisator_t::serialise(buf, obj);
    return polcy.store(buf, size);
  }

  /// @brief load data from storage by storage polcy
  /// If data is invalid, force reset to default
  T load() const {
    auto size = serialisator_t::getSize();
    uint8_t buf[size];

    polcy.load(buf, size);
    return serialisator_t::deserialise(buf);
  }

  template <typename... Tpolicy_args>
  SharedStorageManager(const Tpolicy_args &... policy_args)
      : polcy(policy_args...) {}

private:
  Tpolicy polcy;
};

class MemcpyStoragePolcy {
public:
  MemcpyStoragePolcy(volatile void *addr) : addr(addr) {}

  bool store(const void *data, size_t size) const;
  void load(void *pos, size_t size) const;

protected:
  volatile void *addr;
};

#endif /* _SHARED_STORAGE_H_ */
