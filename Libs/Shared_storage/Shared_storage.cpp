#include "Shared_storage.h"

#include <cstring>

bool MemcpyStoragePolcy::store(const void *data, size_t size) const {
  memcpy((void *)addr, data, size);
  return true;
}

void MemcpyStoragePolcy::load(void *pos, size_t size) const {
  memcpy(pos, (void *)addr, size);
}
