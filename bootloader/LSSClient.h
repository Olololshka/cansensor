#ifndef _LSSCLIENT_H_
#define _LSSCLIENT_H_

#include "Bootloader_App_Shared_data.h"
#include "CANopen.h"

struct LSSClient {
  static bool_t checkBitrate(void *obj, uint16_t bitRate);
  static void applyBitrate(void *obj, uint16_t bitRate);
  static bool_t store_config(void *obj, uint8_t id, uint16_t speed);

  void configure(CO_CANmodule_t *can_context, CO_LSSslave_t *lss_slave_client,
             const uint16_t initial_speed);
  void process(const uint32_t initial_address);

  LinkInfo linkInfo;
  CO_CANmodule_t *can_context;
};

#endif /* _LSSCLIENT_H_ */
