/*
 * CANopen main program file.
 *
 * This file is a template for other microcontrollers.
 *
 * @file        main_generic.c
 * @author      Janez Paternoster
 * @copyright   2004 - 2015 Janez Paternoster
 *
 * This file is part of CANopenNode, an opensource CANopen Stack.
 * Project home page is <https://github.com/CANopenNode/CANopenNode>.
 * For more information on CANopen see <http://www.can-cia.org/>.
 *
 * CANopenNode is free and open source software: you can redistribute
 * it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Following clarification and special exception to the GNU General Public
 * License is included to the distribution terms of CANopenNode:
 *
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library. Thus, the terms and
 * conditions of the GNU General Public License cover the whole combination.
 *
 * As a special exception, the copyright holders of this library give
 * you permission to link this library with independent modules to
 * produce an executable, regardless of the license terms of these
 * independent modules, and to copy and distribute the resulting
 * executable under terms of your choice, provided that you also meet,
 * for each linked independent module, the terms and conditions of the
 * license of that module. An independent module is a module which is
 * not derived from or based on this library. If you modify this
 * library, you may extend this exception to your version of the
 * library, but you are not obliged to do so. If you do not wish
 * to do so, delete this exception statement from your version.
 */

#include "CANopen.h"

#include "BoardInit.h"

#include "Bootloader_App_Shared_data.h"
#include "LSSClient.h"
#include "app_loader.h"

#include "CRC32_platform.h"

CAN_HandleTypeDef can1{.Instance = CAN1};
LSSClient lSSClient;

/* CAN interrupt functions
 * *****************************************************/

//  .word USB_HP_CAN1_TX_IRQHandler
//  .word USB_LP_CAN1_RX0_IRQHandler
//  .word CAN1_RX1_IRQHandler
//  .word CAN1_SCE_IRQHandler

void /* interrupt */ CAN1InterruptHandler_rx(void) {
  CO_CANinterrupt_Rx(CO->CANmodule[0]);
}

void /* interrupt */ CAN1InterruptHandler_tx(void) {
  CO_CANinterrupt_Tx(CO->CANmodule[0]);
}

static void tryStartApplication() {
  auto app = Application::getApplication();
  if (app->verify()) {
    if (CO->CANmodule) {
      // if module initcialised
      CO_delete((int32_t)&can1); // shut down can for bootloader
    }

    app->start();
  }
}

/// @brief Загрузить информацию LSS из заголовка приложения в OD
static void loadIdentity() {
  auto app = Application::getApplication();
  if (app->verify()) {
    OD_identityObject.vendorID = app->get_canopen_vendor_id();
    OD_identityObject.productCode = app->get_canopen_product_id();
    OD_identityObject.revisionNumber = app->get_version();
    OD_identityObject.serialNumber = app->get_serial();
  } else {
    // default values FIXME
    OD_identityObject.vendorID = 0x004f038C;
    OD_identityObject.productCode = 0xCAB1;
    OD_identityObject.revisionNumber = 0;
    OD_identityObject.serialNumber = 1;
  }
}

static void reset_communication(LinkInfo &linkInfo) {
  CO_new(); // создание всех структур
  CO_CANinit((int32_t)&can1, linkInfo.speed); // инициализация железа
  loadIdentity();
  lSSClient.configure(CO->CANmodule[0], CO->LSSslave,
                      linkInfo.speed); // настройка LSS клиента

  CO_CANsetNormalMode(CO->CANmodule[0]); // разрешение работы CAN-модуля

  if (linkInfo.node_ID == CO_LSS_NODE_ID_ASSIGNMENT) {
    // ожидание присвоения адреса по LSS
    lSSClient.process(CO_LSS_NODE_ID_ASSIGNMENT);
    linkInfo = lSSClient.linkInfo;
  }
  CO_CANopenInit(linkInfo.node_ID); // запуск всех остальных служб загрузчика
}

int main(void) {
  InitBoard();

  // Load networc config, reset if invalid
  auto linkInfo = LinkInfoStorage.load();
  if (!linkInfo.checkConfigured()) {
    linkInfo.resetDefaults();

    linkInfo.node_ID = 0x18;
    linkInfo.update_crc();

    LinkInfoStorage.store(linkInfo);
  }

  // Запуск CanOpenNode
  reset_communication(linkInfo);

  __enable_irq();

#if 1

  while (1) {
    auto cmd = CO_process(CO, 1, nullptr);
    switch (cmd) {
    case CO_RESET_COMM: // Application must provide communication reset.
      __disable_irq();
      reset_communication(linkInfo);
      __enable_irq();
      break;
    case CO_RESET_APP: // Application must provide complete device reset
      __disable_irq();
      LinkInfoStorage.store(LinkInfo()); // force LSS obtain address after boot
      SoftReset();
      break;
    case CO_RESET_NOT:  // no action
    case CO_RESET_QUIT: // Application must quit, no reset of microcontroller
                        // ignore - it's bootloader
      break;
    }
  }

#endif

  tryStartApplication();

  // processBootloader()

  SoftReset();
}
