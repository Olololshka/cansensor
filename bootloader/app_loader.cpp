#include <hw_includes.h>

#include "CRC32_platform.h"

#include "app_settings.h"

#include "app_loader.h"

extern const uint32_t _sidata;     // data start (FLASH)
extern const uint32_t _data_start; // data start RAM
extern const uint32_t _data_end;   // data end RAM
extern const uint32_t _estack;     // last valid ram WORD

struct vectors_table_header {
  uint32_t sp_init_state;
  uint32_t reset_vector;
};

#define HEADER_ASSERT(expr)                                                    \
  {                                                                            \
    if (!(expr))                                                               \
      return false;                                                            \
  }

static const uint8_t *align(const uint8_t *ptr, const size_t alignment) {
  const auto over = (size_t)ptr % alignment;
  return over ? (uint8_t *)(((size_t)ptr / alignment + 1) * alignment) : ptr;
}

const Application *const Application::getApplication() {
  auto app_end = &_sidata + (&_data_end - &_data_start);
  return (const Application *const)align((uint8_t *)app_end, FLASH_PAGE_SIZE);
}

bool Application::verify() const {
  HEADER_ASSERT(app_magick == APP_MAGICK);
  HEADER_ASSERT(app_image_start % sizeof(uint32_t) == 0);
  HEADER_ASSERT(app_image_end % sizeof(uint32_t) == 0);
  HEADER_ASSERT(app_image_end > app_image_start);

  HEADER_ASSERT(app_vectors_pos >= app_image_start);
  HEADER_ASSERT(app_vectors_pos < app_image_end);

  const vectors_table_header *vectors = (vectors_table_header *)app_vectors_pos;
  const uint32_t RAM_ORIGIN = (uint32_t)&_sidata;
  const uint32_t RAM_END = (uint32_t)&_estack;

  HEADER_ASSERT(vectors->sp_init_state > RAM_ORIGIN);
  HEADER_ASSERT(vectors->sp_init_state <= RAM_END);

  HEADER_ASSERT(vectors->reset_vector >= app_image_start);
  HEADER_ASSERT(vectors->reset_vector < app_image_end);

  const auto calc_crc32 = CRC32_platform().accamulate(
      (uint8_t *)app_image_start, app_image_end - app_image_start);

  HEADER_ASSERT(calc_crc32 == app_crc32);

  return true;
}

void Application::start() const {
  __disable_irq();

  const vectors_table_header *vectors = (vectors_table_header *)app_vectors_pos;

  asm volatile("mov     sp, %0" : : "r"(vectors->sp_init_state) :);
  asm volatile("ldr     r12, =0xFFFA");
  asm volatile("bx      %0" : : "r"(vectors->reset_vector) :);

  while (1)
    ;
}

uint32_t Application::get_serial() const {
  SharedStorageManager<application_settings, flash_read_polcy>
      app_settings_manager(app_settings);
  return app_settings_manager.load().serial_number;
}
