#include <cstring>

#include "hw_includes.h"

#include "Bootloader_App_Shared_data.h"

#include "CRC32_platform.h"
#include "hw_includes.h"

#define INITIAL_BITRATE 10
#define LSS_NODE_ID_ASSIGNMENT 0xff

static volatile uint8_t Linkinfo_placeholder[sizeof(LinkInfo)]
    __attribute__((section(".SharedStorage.LinkInfo")));

SharedStorageManager<LinkInfo, MemcpyStoragePolcy>
    LinkInfoStorage(&Linkinfo_placeholder);

void LinkInfo::resetDefaults() {
  node_ID = LSS_NODE_ID_ASSIGNMENT;
  speed = INITIAL_BITRATE;
  update_crc();
}

bool LinkInfo::checkConfigured() {
  static const uint16_t speeds[] = {10, 20, 50, 125};

  if (!check_crc())
    return false;

  if (node_ID < 1 || node_ID > 127) {
    return false;
  }

  for (size_t i = 0; i < sizeof(speeds) / sizeof(uint16_t); ++i) {
    if (speeds[i] == speed) {
      return true;
    }
  }
  return false;
}

void LinkInfo::update_crc() {
  crc32 = 0;
  auto newcrc32 =
      CRC32_platform().accamulate((uint8_t *)this, sizeof(LinkInfo));
  crc32 = newcrc32;
}

bool LinkInfo::check_crc() const {
  LinkInfo s(*this);
  s.update_crc();
  return s.crc32 == crc32;
}

////////////////////////////////////////////////////////////////////////////////

void __attribute__((noreturn)) SoftReset() {
#if 0
    SCB->AIRCR  = (uint32_t)((0x5FAUL << SCB_AIRCR_VECTKEY_Pos)    |
                             (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) |
                              SCB_AIRCR_SYSRESETREQ_Msk    );
#endif
  NVIC_SystemReset();
}
