#include "LSSClient.h"

bool_t LSSClient::checkBitrate(void *obj, uint16_t bitRate) {
  UNUSED(obj);
  static const uint16_t bitrates[] = {10, 20, 50, 125}; // уточнить!
  for (size_t i = 0; i < sizeof(bitrates); ++i)
    if (bitrates[i] == bitRate)
      return true;
  return false;
}

void LSSClient::applyBitrate(void *obj, uint16_t bitRate) {
  LSSClient *_this = static_cast<LSSClient *>(obj);
  CO_CANmodule_setBitrate(_this->can_context, bitRate);
}

bool_t LSSClient::store_config(void *obj, uint8_t id, uint16_t speed) {
  LSSClient *_this = static_cast<LSSClient *>(obj);
  _this->linkInfo.node_ID = id;
  _this->linkInfo.speed = speed;
  return true;
}

void LSSClient::configure(CO_CANmodule_t *can_context,
                          CO_LSSslave_t *lss_slave_client,
                          const uint16_t initial_speed) {
  this->can_context = can_context;
  linkInfo.speed = checkBitrate(nullptr, initial_speed) ? initial_speed : 10;

  // основной обработчик событий LSS: CO_LSSslave_receive()
  CO_LSSinit(CO_LSS_NODE_ID_ASSIGNMENT, initial_speed);

  CO_LSSslave_initCheckBitRateCallback(lss_slave_client, this, &checkBitrate);
  CO_LSSslave_initActivateBitRateCallback(lss_slave_client, this,
                                          &applyBitrate);
  CO_LSSslave_initCfgStoreCallback(lss_slave_client, this, &store_config);
}

void LSSClient::process(const uint32_t initial_address) {
  while ((initial_address == linkInfo.node_ID) || (linkInfo.node_ID == 0) ||
         (linkInfo.node_ID > 127))
    ;
}
