#ifndef _Bootloader_App_Shared_data_H_
#define _Bootloader_App_Shared_data_H_

#include <stdint.h>

#include "Shared_storage.h"

struct LinkInfo {
  LinkInfo() { resetDefaults(); }
  void resetDefaults();
  bool checkConfigured();
  void update_crc();

  uint8_t node_ID;
  uint16_t speed;

private:
  uint32_t crc32;

  bool check_crc() const;
} __attribute__((packed));

extern SharedStorageManager<LinkInfo, MemcpyStoragePolcy> LinkInfoStorage;

void __attribute__((noreturn)) SoftReset();

#endif /* _Bootloader_App_Shared_data_H_ */
