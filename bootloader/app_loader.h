#ifndef _APP_LOADER_H_
#define _APP_LOADER_H_

#include <stdint.h>

struct Application {
  static const Application *const getApplication();

  Application() = delete;
  Application(Application &) = delete;

  bool verify() const; // check application header before start
  void __attribute__((noreturn)) start() const;

  uint32_t get_canopen_vendor_id() const { return app_canopen_vendor_id; }
  uint32_t get_canopen_product_id() const { return app_canopen_product_id; }
  uint32_t get_version() const { return app_version; }
  uint32_t get_serial() const;

private:
  // Повторяет структуру из линкерскрипта для приложения
  uint32_t app_magick;
  uint32_t app_image_start;
  uint32_t app_image_end;
  uint32_t app_canopen_vendor_id;
  uint32_t app_canopen_product_id;
  uint32_t app_version;
  uint32_t app_crc32;
  void *app_settings;
  uint32_t app_vectors_pos;
};

#endif /* _APP_LOADER_H_ */
