#ifndef _APP_SETTINGS_H_
#define _APP_SETTINGS_H_

#include "Shared_storage.h"

struct flash_read_polcy : public MemcpyStoragePolcy {
  flash_read_polcy(volatile void *flashAddr) : MemcpyStoragePolcy(flashAddr) {}

  bool store(const void *data, size_t size) const { return false; }
};

struct application_settings {
  uint32_t serial_number;
} __attribute__((packed));

#endif /* _APP_SETTINGS_H_ */
