#include "hw_includes.h"

#include "stm32_gpio_af.h"

#include "BoardInit.h"

#if defined(STM32F1)
// Need c++14! (not working as planned!)

struct sDiv_Mul {
  uint32_t mul, div;
  void runtime_fallback(bool x) {}
  constexpr sDiv_Mul(int32_t m = -1, int32_t d = -1) : mul(m), div(d) {
    if ((m < 0) || (d < 0))
      runtime_fallback(false);
  }
};

constexpr sDiv_Mul calc_pll_div_mul(const uint32_t div_mul) {
  const uint32_t muls[] = {RCC_PLL_MUL2,  RCC_PLL_MUL3,  RCC_PLL_MUL4,
                           RCC_PLL_MUL5,  RCC_PLL_MUL6,  RCC_PLL_MUL7,
                           RCC_PLL_MUL8,  RCC_PLL_MUL9,  RCC_PLL_MUL10,
                           RCC_PLL_MUL11, RCC_PLL_MUL12, RCC_PLL_MUL13,
                           RCC_PLL_MUL14, RCC_PLL_MUL15, RCC_PLL_MUL16};
  const uint32_t divs[] = {
    RCC_HSE_PREDIV_DIV1,
    RCC_HSE_PREDIV_DIV2,
#if defined(STM32F105xC) || defined(STM32F107xC) || defined(STM32F100xB) ||    \
    defined(STM32F100xE)
    RCC_HSE_PREDIV_DIV3,
    RCC_HSE_PREDIV_DIV4,
    RCC_HSE_PREDIV_DIV5,
    RCC_HSE_PREDIV_DIV6,
    RCC_HSE_PREDIV_DIV7,
    RCC_HSE_PREDIV_DIV8,
    RCC_HSE_PREDIV_DIV9,
    RCC_HSE_PREDIV_DIV10,
    RCC_HSE_PREDIV_DIV11,
    RCC_HSE_PREDIV_DIV122,
    RCC_HSE_PREDIV_DIV13,
    RCC_HSE_PREDIV_DIV14,
    RCC_HSE_PREDIV_DIV15,
    RCC_HSE_PREDIV_DIV16
#endif
  };
  for (uint32_t mul = 2; mul < sizeof(muls) / sizeof(uint32_t) + 2; ++mul)
    for (uint32_t div = 1; div < sizeof(divs) / sizeof(uint32_t) + 1; ++div)
      if (mul / div == div_mul) {
        return sDiv_Mul(muls[mul - 2], divs[div - 1]);
      }
  return sDiv_Mul();
}
#endif

void InitOSC() {
#if defined(STM32F1)
  constexpr auto div_mul = calc_pll_div_mul(F_CPU / HSE_VALUE);
  constexpr RCC_OscInitTypeDef RCC_OscInitStruct = {
      // HSE_VALUE -> PREDIV1 -> PLLMUL => F_CPU
      // PREDIV1 / PLLMUL = F_CPU / HSE_VALUE

      .OscillatorType = RCC_OSCILLATORTYPE_HSE,
      .HSEState = RCC_HSE_ON,
      .HSEPredivValue = div_mul.div,
      .LSEState = RCC_LSE_OFF,
      .HSIState = RCC_HSI_OFF,
      .PLL = {.PLLState = RCC_PLL_ON,
              .PLLSource = RCC_PLLSOURCE_HSE,
              .PLLMUL = div_mul.mul}};

#elif defined(STM32L1)
  // HSE_VALUE -> PLLMUL -> PLLDIV => F_CPU

  // PLLMUL = USB_CLK_TARGET / HSE_VALUE
  // PLLDIV = USB_CLK_TARGET / F_CPU

#define __PLLMUL (USB_CLK_TARGET / HSE_VALUE)
#if __PLLMUL == 3
#define _PLLMUL RCC_PLL_MUL3
#elif __PLLMUL == 4
#define _PLLMUL RCC_PLL_MUL4
#elif __PLLMUL == 6
#define _PLLMUL RCC_PLL_MUL6
#elif __PLLMUL == 8
#define _PLLMUL RCC_PLL_MUL8
#elif __PLLMUL == 12
#define _PLLMUL RCC_PLL_MUL12
#elif __PLLMUL == 16
#define _PLLMUL RCC_PLL_MUL16
#elif __PLLMUL == 24
#define _PLLMUL RCC_PLL_MUL24
#elif __PLLMUL == 32
#define _PLLMUL RCC_PLL_MUL32
#elif __PLLMUL == 48
#define _PLLMUL RCC_PLL_MUL48
#else
#error "Correct value of RCC_PLL_MUL not found, check HSE_VALUE value"
#endif

#define __PLLDIV (USB_CLK_TARGET / F_CPU)
#if __PLLDIV == 2
#define _PLLDIV RCC_PLL_DIV2
#elif _PLLDIV == 3
#define _PLLMUL RCC_PLL_DIV3
#elif _PLLDIV == 4
#define _PLLMUL RCC_PLL_DIV4
#else
#error "Correct value of RCC_PLL_DIV not found, check F_CPU value"
#endif

  RCC_OscInitTypeDef RCC_OscInitStruct = {
      .OscillatorType = RCC_OSCILLATORTYPE_HSE,
      .HSEState = RCC_HSE_ON,
      .LSEState = RCC_LSE_OFF,
      .HSIState = RCC_HSI_OFF,
      .PLL = {.PLLState = RCC_PLL_ON,
              .PLLSource = RCC_PLLSOURCE_HSE,
              .PLLMUL = _PLLMUL,
              .PLLDIV = _PLLDIV}};
#else
#error "Unsupported CPU family"
#endif
  HAL_RCC_OscConfig((RCC_OscInitTypeDef *)&RCC_OscInitStruct);
}

void Configure_AHB_Clocks() {
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK |
                                RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;

  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;

#if defined(STM32F1)
  RCC_ClkInitStruct.APB1CLKDivider =
      F_CPU > 36000000U ? RCC_HCLK_DIV2 : RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
#elif defined(STM32L1)
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);
#else
#error "Unsupported CPU family"
#endif
}

// Set up board clocks
void SystemClock_Config(void) {
  InitOSC();
  Configure_AHB_Clocks();

  // Set up SysTTick to 1 ms
  // TODO: Do we really need this? SysTick is initialized multiple times in HAL
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  // SysTick_IRQn interrupt configuration - setting SysTick as lower priority to
  // satisfy FreeRTOS requirements
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

void SetupInterruptPrioritys() {
#if defined(STM32F1)
#elif defined(STM32L1)
  const IRQn_Type list[] = {DMA1_Channel1_IRQn, DMA1_Channel2_IRQn,
                            DMA1_Channel3_IRQn, DMA1_Channel4_IRQn,
                            DMA1_Channel5_IRQn, DMA1_Channel6_IRQn,
                            DMA1_Channel7_IRQn};
  for (auto irq = 0; irq < sizeof(list) / sizeof(IRQn_Type); ++irq)
    NVIC_SetPriority(list[irq], 90);
#else
#error "SETUP interrupt prioritys first!"
#endif
}

void InitBoard() {
  // Initialize board and HAL
  HAL_Init();

  // __NVIC_PRIO_BITS - how many prioruty bits avalable
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  SystemClock_Config();
  SetupInterruptPrioritys();
}

#ifdef STM32L1
extern "C" uint32_t HAL_PWREx_GetVoltageRange(void) {
  return PWR->CR & PWR_CR_VOS;
}
#endif

extern "C" void HAL_CAN_MspInit(CAN_HandleTypeDef *hcan) {
#if defined(STM32F1)
  GPIO_InitTypeDef GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/

  /* Enable GPIO clock ****************************************/
  __HAL_RCC_GPIOB_CLK_ENABLE();
  /* CAN1 Periph clock enable */
  __HAL_RCC_CAN1_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* CAN1 TX GPIO pin configuration */
  GPIO_InitStruct.Pin = 9;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* CAN1 RX GPIO pin configuration */
  GPIO_InitStruct.Pin = 8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  __HAL_RCC_AFIO_CLK_ENABLE();
  __HAL_AFIO_REMAP_CAN1_2(); // remap CAN1 to PB8,9
#endif
}
