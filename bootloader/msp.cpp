
#include "hw_includes.h"

extern "C" void HAL_CAN_MspInit(CAN_HandleTypeDef *hcan) {
  GPIO_InitTypeDef GPIO_InitStruct;
  if (hcan->Instance == CAN1) {
    /* Peripheral clock enable */
    __HAL_RCC_CAN1_CLK_ENABLE();
    /**CAN1 GPIO Configuration
     *  PB8 ------> CAN1_RX
     *  PB9 ------> CAN1_TX
     */
    GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

#if 0
    /* Peripheral interrupt init */
    HAL_NVIC_SetPriority(CAN1_TX_IRQn, 2, 2);
    HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);
#endif
  }
}

extern "C" void HAL_CAN_MspDeinit(CAN_HandleTypeDef *hcan) {
  if (hcan->Instance == CAN1) {
    /* Peripheral clock disable */
    __HAL_RCC_CAN1_CLK_DISABLE();
  }
}
