#!/bin/python
########################################################
#
#  Memory Analyzer stm32
#  Author:  S
#  Based on https://github.com/Sermus/ESP8266_memory_analyzer
#
########################################################

import os.path
import shlex
import subprocess
import sys

from common import numerize

ROM_SECTION = 1 << 0
RAM_SECTION = 1 << 1
RAM_ROM_INITED = ROM_SECTION | RAM_SECTION

sections = [
    ("text", "Code", ROM_SECTION),
    ("rodata", "ReadOnly Data", ROM_SECTION),
    ("ARM", "ARM sections", ROM_SECTION),
    ("static_cxx", "C++ static objects", ROM_SECTION),

    ("data", "Initialized Data", RAM_ROM_INITED),
    ("bss", "Uninitialized Data", RAM_SECTION)
]

if len(sys.argv) < 5:
    print("Usage: \n\t%s <path_to_objdump> <path_to_app_out> <ROM Size> <RAM Size>" % sys.argv[0])
    sys.exit(1)

objectDumpBin = sys.argv[1]
appOut        = sys.argv[2]

TOTAL_ROM     = numerize(sys.argv[3])
TOTAL_RAM     = numerize(sys.argv[4])

if not os.path.exists(appOut):
    print("Cannot find application out file: %s" % appOut)
    sys.exit(1)

print("Memory usage info: %s" % appOut)
command = "%s -t '%s' " % (objectDumpBin, appOut)
response = subprocess.check_output(shlex.split(command))
if isinstance(response, bytes):
    response = response.decode('utf-8')
lines = response.split('\n')

print("{0: >10}|{1: >28}|{2: >12}|{3: >12}|{4: >8}".format("Section", "Description", "Start (hex)", "End (hex)", "Used space"));
print("----------------------------------------------------------------------------");

usedRAM = 0;
usedROM = 0;

i = 0
for (name, descr, region) in sections:
    sectionStartToken = " _%s_start" %  name
    sectionEndToken   = " _%s_end" % name
    sectionStart = -1;
    sectionEnd = -1;
    for line in lines:
        if sectionStartToken in line:
            data = line.split(' ')
            sectionStart = int(data[0], 16)

        if sectionEndToken in line:
            data = line.split(' ')
            sectionEnd = int(data[0], 16)

        if sectionStart != -1 and sectionEnd != -1:
            break

    sectionLength = sectionEnd - sectionStart
    if region & ROM_SECTION:
        usedROM += sectionLength;
    if region & RAM_SECTION:
        usedRAM += sectionLength

    print("{0: >10}|{1: >28}|{2:12X}|{3:12X}|{4:8}".format(name, descr, sectionStart, sectionEnd, sectionLength))
    i += 1

print("Total Used ROM : %d" % usedROM)
print("Total Used RAM : %d" % usedRAM)
print("Free ROM : %d" % (TOTAL_ROM - usedROM))
print("Free RAM : %d" % (TOTAL_RAM - usedRAM))
