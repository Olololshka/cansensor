#!/usr/bin/env python

import re
import sys
import time
import math

from ast import literal_eval
from common import numerize2

if len(sys.argv) < 8:
    print("Usage: \n\t%s <SRC_LD> <DEST_LD> <SYMBOL> <ROM_ORIGIN> <APP_ORIGIN_OFFSET_FILE> <VERSION_FILE> <CODE_HASH_FILE>" % sys.argv[0], file=sys.stderr)
    sys.exit(1)

SRC_LD  = sys.argv[1]
DEST_LD = sys.argv[2]
SYMBOL  = sys.argv[3]
ROM_ORIGIN       = literal_eval(sys.argv[4])
APP_ORIGIN_OFFSET_FILE  = sys.argv[5]
VERSION_FILE     = sys.argv[6]
CODE_HASH_FILE   = sys.argv[7]

with open(APP_ORIGIN_OFFSET_FILE, 'r') as f:
    APP_ORIGIN = int(f.readline())

with open(VERSION_FILE, 'r') as f:
    VERSION = int(f.readline(), 16)

if CODE_HASH_FILE == 'ZERO':
    CODE_HASH = 0
else:
    with open(CODE_HASH_FILE, 'r') as f:
        CODE_HASH = int(f.readline())

with open(SRC_LD, 'r') as f:
    data = f.read()

data = data.replace(SYMBOL + "APP_ORIGIN" + SYMBOL, str(ROM_ORIGIN + APP_ORIGIN))
data = data.replace(SYMBOL + "CODE_HASH" + SYMBOL, str(CODE_HASH))
data = data.replace(SYMBOL + "VERSION" + SYMBOL, str(VERSION))

with open(DEST_LD, 'w') as fo:
    fo.write(data)

print('#include <stdint.h>')
print('const uint32_t __APP_ORIGIN = %d;' % APP_ORIGIN)
print('const uint32_t __CODE_HASH = %d;' % CODE_HASH)
print('const uint64_t __BUILD_TIMESTAMP = %dL;' % math.ceil(time.time()))
print('const uint32_t __BUILD_VERSION = %d;' % VERSION)
