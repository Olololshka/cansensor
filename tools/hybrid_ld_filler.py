#!/usr/bin/env python

import re
import sys
import time
import math

from ast import literal_eval

if len(sys.argv) < 6:
    print("Usage: \n\t%s <SRC_LD> <DEST_LD> <SYMBOL> <ROM_ORIGIN> <APP_ORIGIN_OFFSET_FILE>" % sys.argv[0], file=sys.stderr)
    sys.exit(1)

SRC_LD  = sys.argv[1]
DEST_LD = sys.argv[2]
SYMBOL  = sys.argv[3]
ROM_ORIGIN       = literal_eval(sys.argv[4])
APP_ORIGIN_OFFSET_FILE  = sys.argv[5]

with open(APP_ORIGIN_OFFSET_FILE, 'r') as f:
    APP_ORIGIN = int(f.readline())

with open(SRC_LD, 'r') as f:
    data = f.read()

data = data.replace(SYMBOL + "APP_ORIGIN" + SYMBOL, "0x%08X" % (ROM_ORIGIN + APP_ORIGIN))

with open(DEST_LD, 'w') as fo:
    fo.write(data)
