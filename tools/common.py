
def numerize(s):
    multipliers = {'K': 10**3, 'M': 10**6, 'B': 10**9}

    if s[-1] in multipliers:
        return int(s[:-1]) * multipliers[s[-1]]
    else:
        return int(s)

def numerize2(s):
    multipliers = {'K': 2**10, 'M': 2**20, 'B': 2**30}

    if s[-1] in multipliers:
        return int(s[:-1]) * multipliers[s[-1]]
    else:
        return int(s)
