#!/usr/bin/env python3

import os.path
import shlex
import subprocess
import sys
import re
import math

from common import numerize2

def align(value, border):
    return math.ceil(float(value) / border) * border

if len(sys.argv) < 5:
    print("Usage: \n\t%s <path_to_readelf> <path_to_app_out> <FLASH Size> <FLASH_PAGE_SIZE>" % sys.argv[0], file=sys.stderr)
    sys.exit(1)

objectDumpBin = sys.argv[1]
appOut        = sys.argv[2]
TOTAL_ROM     = numerize2(sys.argv[3])
ALIGNMENT     = numerize2(sys.argv[4])

if not os.path.exists(appOut):
    print("Cannot find application out file: %s" % appOut, file=sys.stderr)
    sys.exit(1)

pattern = re.compile("PROGBITS *([0-9a-f]+) [0-9a-f]+ ([0-9a-f]+)")

command = "%s -t '%s' " % (objectDumpBin, appOut)
response = subprocess.check_output(shlex.split(command))
if isinstance(response, bytes):
    response = response.decode('utf-8')

s = 0
for line in response.split('\n'):
    m = pattern.search(line)
    if m and m.group(1) != "00000000":
        s += int(m.group(2), 16)


print(align(s, ALIGNMENT))
