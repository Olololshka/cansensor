using System;  
using libEDSsharp;

namespace OD_src_generator {

class main
{
    static int Main(string[] args)
    {
        if (args.Length < 2) {
            Console.WriteLine(
                        string.Format("Usage: {0} <eds-file> <output dir>",
                            System.AppDomain.CurrentDomain.FriendlyName));
            return 1;
        }


        var eds = new EDSsharp();
        eds.Loadfile(args[0]);

        var exporter = new CanOpenNodeExporter();
        exporter.export(args[1], "0", eds);

        return 0;
    }
}

}
