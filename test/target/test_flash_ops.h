#ifndef TEST_FLASH_OPS_H
#define TEST_FLASH_OPS_H

#include "itestsuite.h"

#include "hw_includes.h"

static __attribute__((section(".settings.marker"))) uint8_t d = 0;

static constexpr size_t size2page_count(size_t size) {
  return size % FLASH_PAGE_SIZE > 0 ? size / FLASH_PAGE_SIZE + 1
                                    : size / FLASH_PAGE_SIZE;
}

TEST_SUITE_NAME(test_flash_ops)

static void flash_programm_start() { HAL_FLASH_Unlock(); }
static void flash_programm_finalise() { HAL_FLASH_Lock(); }

static bool flash_erase(uint32_t start_address, size_t length) {
  FLASH_EraseInitTypeDef er{FLASH_TYPEERASE_PAGES, FLASH_BANK_1, start_address,
                            size2page_count(length)};
  uint32_t t;
  auto res = HAL_FLASHEx_Erase(&er, &t);
  return HAL_OK == res;
}

static bool flash_write(uint32_t dest, const uint8_t *data, size_t size) {
  unsigned int i;

  while (FLASH->SR & FLASH_SR_BSY)
    ;
  if (FLASH->SR & FLASH_SR_EOP) {
    FLASH->SR = FLASH_SR_EOP;
  }

  FLASH->CR |= FLASH_CR_PG;

  for (i = 0; i < size; i += 2) {
    *(volatile unsigned short *)(dest + i) =
        (((unsigned short)data[i + 1]) << 8) + data[i];
    while (!(FLASH->SR & FLASH_SR_EOP))
      ;
    FLASH->SR = FLASH_SR_EOP;
  }

  if (i > size) {
    // 1 byte remaning
    uint16_t d = data[size - 1];
    *(volatile unsigned short *)(dest + i) = d;
    while (!(FLASH->SR & FLASH_SR_EOP))
      ;
    FLASH->SR = FLASH_SR_EOP;
  }

  FLASH->CR &= ~(FLASH_CR_PG);

  return true;
}

static void test_erease_single_page() {
  flash_programm_start();
  flash_erase((uint32_t)&d, FLASH_PAGE_SIZE);
  flash_programm_finalise();
}

static void test_write_single_page() {
  uint8_t data[16];
  for (uint8_t i = 0; i < sizeof(data); ++i) {
    data[i] = i;
  }
  flash_programm_start();
  flash_write((uint32_t)&d, data, sizeof(data));
  flash_programm_finalise();
}

void run() override {
  RUN(test_erease_single_page);
  RUN(test_write_single_page);
}
}
;

#endif // TEST_FLASH_OPS_H
