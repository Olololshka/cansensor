#ifndef _CRC_BENCH_H_
#define _CRC_BENCH_H_

#include <cstring>

#include "hw_includes.h"

#include "itestsuite.h"

#include "CRC32_platform.h"
#include "CRC32_software.h"

#include "crc16-ccitt.h"

uint32_t crc32_halfbyte(const void *data, size_t length,
                        uint32_t previousCrc32 = 0) {
  uint32_t crc = ~previousCrc32;
  unsigned char *current = (unsigned char *)data;
  static uint32_t lut[16] = {0x00000000, 0x1DB71064, 0x3B6E20C8, 0x26D930AC,
                             0x76DC4190, 0x6B6B51F4, 0x4DB26158, 0x5005713C,
                             0xEDB88320, 0xF00F9344, 0xD6D6A3E8, 0xCB61B38C,
                             0x9B64C2B0, 0x86D3D2D4, 0xA00AE278, 0xBDBDF21C};

  while (length--) {
    crc = lut[(crc ^ *current) & 0x0F] ^ (crc >> 4);
    crc = lut[(crc ^ (*current >> 4)) & 0x0F] ^ (crc >> 4);
    current++;
  }
  return ~crc;
}

TEST_SUITE_NAME(crc_bench)
public:
static void calc_crc16_table() {
  auto res = crc16_ccitt(input_data, sizeof(input_data), 0);
  ASSERT_EQUALS(res, crc16_correct);
}

static void calc_crc32_table() {
  auto res = crc32_halfbyte(input_data, sizeof(input_data));
  ASSERT_EQUALS(res, crc32_correct);
}

template <typename Tcalculator> static void calc_crc32() {
  Tcalculator calculator;
  auto res = calculator.accamulate(input_data, sizeof(input_data));
  ASSERT_EQUALS(res, crc32_correct);
}

void run() override {
  auto getticks = []() { return SysTick->VAL; };

  memset(input_data, 0, sizeof(input_data));

  using test_t = void (*)();

  static const test_t tests[] = {calc_crc16_table, calc_crc32_table,
                                 calc_crc32<CRC32_software>,
                                 calc_crc32<CRC32_platform>};

  volatile uint32_t times[sizeof(tests) / sizeof(test_t)];
  HAL_SYSTICK_Config(0xFFFFFF);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  for (uint32_t i = 0; i < sizeof(tests) / sizeof(test_t); ++i) {
    auto start = getticks();
    RUN(tests[i]);
    times[i] = start - getticks();
  }

  // only for watch result
  volatile uint64_t summ = 0;
  for (uint32_t i = 0; i < sizeof(tests) / sizeof(test_t); ++i) {
    summ += times[i];
  }
}

private:
static uint8_t input_data[1024];
static const uint16_t crc16_correct = 9705;
static const uint32_t crc32_correct = 265403464;
}
;

uint8_t crc_bench::input_data[1024];

#endif /* _CRC_BENCH_H_ */
