#ifndef TEST_FLASH_OPS_H
#define TEST_FLASH_OPS_H

#include "itestsuite.h"

#include "hw_includes.h"

#include "Flash_RW_polcy.h"

#include <cstring>

static __attribute__((section(".settings.marker"))) uint8_t d = 0;

TEST_SUITE_NAME(test_flash_rw_polcy)

void test_flashing() {
  uint8_t buf[9]; // sizeof(buf) % sizeof(uint16_t) != 0

  Flash_RW_polcy polcy(&d);
  polcy.load(buf, sizeof(buf));

  memset(buf + 1, 0xff, sizeof(buf));
  buf[0]++;
  buf[4] = 9;
  buf[8] = 0xAA;

  polcy.store(buf, sizeof(buf));
}

void run() override { RUN_METHOD(&test_flash_rw_polcy::test_flashing); }
}
;

#endif // TEST_FLASH_OPS_H
