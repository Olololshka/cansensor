#ifndef ITESTSUITE_H
#define ITESTSUITE_H

#include <memory>

#define tt_printf(...)
#include "tinytest.h"

#define TEST_SUITE_NAME(name)                                                  \
  class name : public ITestSuite {                                             \
  public:                                                                      \
    const char *testname() const override { return #name; }                    \
    static std::unique_ptr<ITestSuite> instance() {                            \
      return std::unique_ptr<ITestSuite>(new name);                            \
    }                                                                          \
                                                                               \
  private:

class ITestSuite {
public:
  virtual const char *testname() const = 0;
  virtual void run() = 0;
  void start() {
    tt_printf("Executing %s...\n", testname());
    run();
  }
};

#endif // ITESTSUITE_H
