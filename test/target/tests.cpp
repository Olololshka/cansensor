#include <Arduino_FreeRTOS.h>

#include "itestsuite.h"

// Вот эти инклюды менять местами нельзя - все сломается!
#include "test_flash_rw_polcy.h"

#include "crc_bench.h"

void Prepere_tests() {}

int main() {
  Prepere_tests();

  crc_bench::instance()->start();
  // test_flash_rw_polcy::instance()->start();

  TEST_REPORT();

  return 0;
}
