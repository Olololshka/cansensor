#ifndef _TEST_TEMPLATE_SPECIFICATION_H_
#define _TEST_TEMPLATE_SPECIFICATION_H_

#include <cxxtest/TestSuite.h>

#include "Shared_storage.h"

class test_template_specification : public CxxTest::TestSuite {
public:
  void test1() {
    struct testStruct {
      uint32_t foo;
      uint8_t bar;
    };

    testStruct ts;
    MemcpyStoragePolcy p(&ts);
    SharedStorageManager<testStruct> ss(p);

    ss.store({10, 5});
    ts = ss.load();
  }
};

#endif /* _TEST_TEMPLATE_SPECIFICATION_H_ */
