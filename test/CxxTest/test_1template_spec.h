#ifndef TEST_1TEMPLATE_SPEC_H
#define TEST_1TEMPLATE_SPEC_H

#include <cxxtest/TestSuite.h>

#include <type_traits>

// Это основной шаблон, он не должен быть инстанцирован, считай, что он
// pure-virtual
template <typename T, // это тип, с которым будем работать
          typename Enable =
              void // это чисто для выбора нужной реализации, оставь так!
          >
struct A {
  /* иногда в целях отладки полезно бывает добавить ему пустое тело,
   * тогда сообщения об ошибках станут понятнее */
};

// это щаблон, который специализирует базовый
template <typename TStruct> // это новый параметр шаблона
struct A<TStruct, // передаём его в базовый шаблон
         typename std::enable_if<
             std::is_pod<TStruct>::value>::type> // эта штука являетсядетектором
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
// Тонкий момент: вот тут не надо указывать тип, возвращаемый enable_if,
// иначе все сломается
{
  // заполняем тело некоторой реализацией для A<T = TStruct>
  int foo() { return 2; }
};

class test_1template_spec : public CxxTest::TestSuite {
public:
  void test_meta_if() {
    struct S {};

    TS_ASSERT_EQUALS(std::is_integral<int>::value, true);
    TS_ASSERT_EQUALS(std::is_integral<S>::value, false);
  }

  void test_pod() {
    // данный тест демонстрирует то, что написано в
    // https://msdn.microsoft.com/ru-ru/library/bb982918.aspx
    // а если в кратце. Все простые типы, тируктуры и массивы из них - pod

    struct pod_struct {
      int a;
    };

    enum en { element };

    TS_ASSERT_EQUALS(std::is_pod<int>::value, true);
    TS_ASSERT_EQUALS(std::is_pod<double>::value, true);
    TS_ASSERT_EQUALS(std::is_pod<char[10]>::value, true);
    TS_ASSERT_EQUALS(std::is_pod<pod_struct>::value, true);
    TS_ASSERT_EQUALS(std::is_pod<en>::value, true);

    // non-pod

    // наследование
    struct nonpod1 : public pod_struct {
      char foo;
    };

    // определение конструктора/деструктора и тп...
    struct nonpod2 {
      nonpod2() {}
    };

    // поля - ссылки
    struct nonpod3 {
      int &a;
    };

    TS_ASSERT_EQUALS(std::is_pod<nonpod1>::value, false);
    TS_ASSERT_EQUALS(std::is_pod<nonpod2>::value, false);
    TS_ASSERT_EQUALS(std::is_pod<nonpod3>::value, false);
  }

  void test_template_select() {
    struct pod_struct {
      int p1;
    };

    auto a = A<int>().foo();
    auto b = A<pod_struct>().foo();

    TS_ASSERT_EQUALS(a, 2);
    TS_ASSERT_EQUALS(b, 2);
  }
};

#endif // TEST_1TEMPLATE_SPEC_H
