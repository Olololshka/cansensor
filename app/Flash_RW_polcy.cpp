#include "Flash_RW_polcy.h"

#include <cstring>

#include "hw_includes.h"

Flash_RW_polcy::Flash_RW_polcy(volatile void *addr)
    : MemcpyStoragePolcy(addr) {}

bool Flash_RW_polcy::store(const void *data, size_t size) const {
  if (memcmp((void *)addr, data, size) != 0) {
    flash_programm_start();

    flash_erase((uint32_t)addr, size);
    flash_write((uint32_t)addr, (uint8_t *)data, size);

    flash_programm_finalise();
  }
  return true;
}

constexpr size_t Flash_RW_polcy::size2page_count(size_t size) {
  return size % FLASH_PAGE_SIZE > 0 ? size / FLASH_PAGE_SIZE + 1
                                    : size / FLASH_PAGE_SIZE;
}

void Flash_RW_polcy::flash_programm_start() const { HAL_FLASH_Unlock(); }

void Flash_RW_polcy::flash_programm_finalise() const { HAL_FLASH_Lock(); }

bool Flash_RW_polcy::flash_erase(uint32_t start_address, size_t length) {
  FLASH_EraseInitTypeDef er{FLASH_TYPEERASE_PAGES, FLASH_BANK_1, start_address,
                            size2page_count(length)};
  uint32_t t;
  auto res = HAL_FLASHEx_Erase(&er, &t);
  return HAL_OK == res;
}

bool Flash_RW_polcy::flash_write(uint32_t dest, const uint8_t *data,
                                 size_t size) {
  unsigned int i;

  while (FLASH->SR & FLASH_SR_BSY)
    ;
  if (FLASH->SR & FLASH_SR_EOP) {
    FLASH->SR = FLASH_SR_EOP;
  }

  FLASH->CR |= FLASH_CR_PG;

  for (i = 0; i < size; i += 2) {
    *(volatile unsigned short *)(dest + i) =
        (((unsigned short)data[i + 1]) << 8) + data[i];
    while (!(FLASH->SR & FLASH_SR_EOP))
      ;
    FLASH->SR = FLASH_SR_EOP;
  }

  FLASH->CR &= ~(FLASH_CR_PG);

  return true;
}
