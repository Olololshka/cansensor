#include "settings_manager.h"

#include "hw_includes.h"

static application_settings __attribute__((section(".settings.app")))
settings_placeholder{0x04, 0};

SharedStorageManager<application_settings, Flash_RW_polcy>
    SettingsManager(&settings_placeholder);
