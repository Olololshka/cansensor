#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"

//
// Define the 'new' operator for C++ to use the freeRTOS memory management
// functions. THIS IS NOT OPTIONAL!
//
void *operator new(size_t size) {
  void *p = pvPortMalloc(size);
  return p;
}

//
// Define the 'delete' operator for C++ to use the freeRTOS memory management
// functions. THIS IS NOT OPTIONAL!
//
void operator delete(void *p) { vPortFree(p); }

namespace std {
//
// Trap for std::function bad call
//
void __throw_bad_function_call(void) {
  taskDISABLE_INTERRUPTS();

  __asm__("BKPT");
}
} // namespace std

// C++14 additional delete operators

#if __cplusplus >= 201402L

__attribute__((weak)) void operator delete(void *ptr, std::size_t) noexcept {
  ::operator delete(ptr);
}

__attribute__((weak)) void operator delete[](void *ptr, std::size_t) noexcept {
  ::operator delete(ptr);
}
#endif
