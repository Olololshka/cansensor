#include <Arduino_FreeRTOS.h>
#include <cstring>

#include "BoardInit.h"
#include "settings_manager.h"

#include "Bootloader_App_Shared_data.h"

static void SetISR_Table() {
  extern uint32_t _isr_vector_start;
  extern const uint32_t _isr_vector_end;
  extern const uint32_t __vectors_pos;

  memcpy(&_isr_vector_start, &__vectors_pos,
         (uint32_t)&_isr_vector_end - (uint32_t)&_isr_vector_start);

  SCB->VTOR = (__IOM uint32_t)&_isr_vector_start;
}

int main(void) {
  /// Оставлять в этом стеке используемые объекты нельзя

  // InitBoard(); // Считаем, что бутлоадер все инициализировал без нас
  SetISR_Table();

  auto linkInfo = LinkInfoStorage.load();
  if (!linkInfo.checkConfigured())
    SoftReset();

  auto settings = SettingsManager.load();
  settings.counter++;
  SettingsManager.store(settings);

  settings.counter++;

  SoftReset();

  // initDebugSerial();

  portENABLE_INTERRUPTS(); // To allow use halt() and HAL_Delay()

  // Creating threads
  /*xTaskCreate(vButtonsThread, "Buttons Thread", configMINIMAL_STACK_SIZE,
     NULL, tskIDLE_PRIORITY + 3, NULL);*/

  // Run scheduler and all the threads
  vTaskStartScheduler();

  // Never going to be here
  return 0;
}

// halt weak
__attribute__((weak)) void halt(uint8_t status) {
  while (1)
    ;
}
