#ifndef FLASH_RW_POLCY_H
#define FLASH_RW_POLCY_H

#include "Shared_storage.h"

struct Flash_RW_polcy : public MemcpyStoragePolcy {
  Flash_RW_polcy(volatile void *addr);

  bool store(const void *data, size_t size) const;

  static constexpr size_t size2page_count(size_t size);

private:
  void flash_programm_start() const;
  void flash_programm_finalise() const;
  static bool flash_erase(uint32_t start_address, size_t length);
  static bool flash_write(uint32_t dest, const uint8_t *data, size_t size);
};

#endif // FLASH_RW_POLCY_H
