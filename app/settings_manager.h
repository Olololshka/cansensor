#ifndef _APP_SETTINGS_H_
#define _APP_SETTINGS_H_

#include "Flash_RW_polcy.h"
#include "Shared_storage.h"

struct application_settings {
  uint32_t serial_number;
  uint32_t counter;
} __attribute__((packed));

extern SharedStorageManager<application_settings, Flash_RW_polcy>
    SettingsManager;

#endif /* _APP_SETTINGS_H_ */
