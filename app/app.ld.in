ENTRY(Reset_Handler)
_estack = @STM32_RAM_ORIGIN@ + @STM32_RAM_SIZE@ - 1;
_Min_Heap_Size = @STM32_MIN_HEAP_SIZE@;
_Min_Stack_Size = @STM32_MIN_STACK_SIZE@;
MEMORY
{
  FLASH (rx)      : ORIGIN = +APP_ORIGIN+, LENGTH = @STM32_FLASH_SIZE@ -
                    (+APP_ORIGIN+ - @STM32_FLASH_ORIGIN@)
  RAM (xrw)       : ORIGIN = @STM32_RAM_ORIGIN@, LENGTH = @STM32_RAM_SIZE@
  @STM32_CCRAM_DEF@
}
SECTIONS
{
  .header :
  {
    LONG(@APP_MAGICK@)  /* Application magick value */
    LONG(_text_start)   /* Application image start address */
    LONG(__vectors_pos + SIZEOF(.isr_vectors)
                       + SIZEOF(.data)) /* application image end address */
    LONG(@CANOPEN_VENDOR_ID@) /* CanOpen VendorID of this product */
    LONG(@CANOPEN_PRODUCT_ID@) /* CanOpen ProductID of this product */
    LONG(+VERSION+)     /* application version: git log -1 --format=%h */
    LONG(+CODE_HASH+)   /* application crc32 */
    LONG(__settings_pos)/* pointer to settings */
    LONG(__vectors_pos) /* pointer to vectors table */
    . = ALIGN(16);
  } > FLASH

  .text :
  {
    _text_start = .;
    *(.text)
    *(.text*)
    *(.glue_7)
    *(.glue_7t)
    *(.eh_frame)
    KEEP (*(.init))
    KEEP (*(.fini))
    . = ALIGN(8);
    _text_end = .;
    _etext = .;
  } >FLASH

  .rodata :
  {
    . = ALIGN(4);
    _rodata_start = .;
    *(.rodata)
    *(.rodata*)
    . = ALIGN(4);
    _rodata_end = .;
  } >FLASH
  _ARM_start = .;
  .ARM.extab   : {
    *(.ARM.extab* .gnu.linkonce.armextab.*) 
  } >FLASH
  .ARM : {
    __exidx_start = .;
    *(.ARM.exidx*)
    __exidx_end = .;
  } >FLASH
  _ARM_end = .;
  _static_cxx_start = .;
  .preinit_array     :
  {
    PROVIDE_HIDDEN (__preinit_array_start = .);
    KEEP (*(.preinit_array*))
    PROVIDE_HIDDEN (__preinit_array_end = .);
  } >FLASH
  .init_array :
  {
    PROVIDE_HIDDEN (__init_array_start = .);
    KEEP (*(SORT(.init_array.*)))
    KEEP (*(.init_array*))
    PROVIDE_HIDDEN (__init_array_end = .);
  } >FLASH
  .fini_array :
  {
    PROVIDE_HIDDEN (__fini_array_start = .);
    KEEP (*(SORT(.fini_array.*)))
    KEEP (*(.fini_array*))
    PROVIDE_HIDDEN (__fini_array_end = .);
  } >FLASH
  _sidata = LOADADDR(.data);
  _static_cxx_end = .;

  __vectors_pos = .;
  .isr_vectors : {
      _isr_vector_start = .;
      KEEP(*(.isr_vector))
      . = ALIGN(4);
      _isr_vector_end = .;
  } >RAM AT> FLASH

  .shared_storage (NOLOAD):
  {
    . = ALIGN(4);
    KEEP(*(.SharedStorage*));
    . = ALIGN(4);
  } >RAM

  .data : 
  {
    _sdata = .;
    _data_start = .;
    . = ALIGN(4);
    *(.data)
    *(.data*)
    . = ALIGN(16);
    _data_end = .;
    _edata = .;
  } >RAM AT> FLASH
  @STM32_CCRAM_SECTION@

  .settings :
  {
    . = ALIGN(@STM32_FLASH_PAGE_SIZE@);
    __settings_pos = .;
    KEEP(*(.settings*))
    __end = .;
  } > FLASH

  .bss :
  {
    _sbss = .;
    __bss_start__ = _sbss;
    _bss_start = .;
    *(.bss)
    *(.bss*)
    *(COMMON)
    . = ALIGN(4);
    _ebss = .;
    __bss_end__ = _ebss;
    _bss_end = .;
  } >RAM

  ._user_heap_stack :
  {
    . = ALIGN(4);
    PROVIDE ( end = . );
    PROVIDE ( _end = . );
    . = . + _Min_Heap_Size;
    . = . + _Min_Stack_Size;
    . = ALIGN(4);
  } >RAM

  /DISCARD/ :
  {
    libc.a ( * )
    libm.a ( * )
    libgcc.a ( * )
  }
  .ARM.attributes 0 : { *(.ARM.attributes) }
}
